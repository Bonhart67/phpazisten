<?php

  $points = 0;
  
  $pw = $_GET["Password"];

  if($pw != null) $points = password($pw);

  function password($pw) {
    $split = str_split($pw);
    if(isNumberInPw($split)) $points++;
    if(isAlphabeticInPw($split)) $points++;
    if(isSpecialInPw($split)) $points+=2;
    if(isLowerInPw($split) && isUpperInPw($split)) $points++;
    return $points;
  }

  function isNumberInPw($pw) {
    foreach ($pw as $c) {
      if ($c >= 0 && $c <= 9) return true;
    }
    return false;
  }

  function isAlphabeticInPw($pw) {
    foreach ($pw as $c) {
      if (ctype_alpha($c)) return true;
    }
    return false;
  }

  function isSpecialInPw($pw) {
    foreach ($pw as $c) {
      if ($c == '?' || $c == '!' || $c == ',' || $c == '+' || $c == '-' || $c == '*' || $c == '/') return true;
    }
    return false;
  }

  function isLowerInPw($pw) {
    foreach ($pw as $c) {
      if (ctype_lower($c)) return true;
    }
    return false;
  }

  function isUpperInPw($pw) {
    foreach ($pw as $c) {
      if (ctype_upper($c)) return true;
    }
    return false;
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <form action="">
    <input type="text" name="Password">
    <input type="submit" value="submit"> 
  </form>
</body>
</html>